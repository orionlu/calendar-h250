import Vue from 'vue';
import App from './App.vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Calendar from 'vue2-event-calendar';
import Flickity from 'vue-flickity';


Vue.component('Calendar', Calendar);
Vue.component('Flickity',Flickity);
Vue.use(ElementUI);

const app = new Vue({
  el: '#app',
  render: h => h(App)
});